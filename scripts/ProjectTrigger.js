// { data, type, prefix, newData }
const ProjectResources = await SDK.Table.get('project_resources');
const Account = await SDK.Table.get('accounts');
let account = await Account.find({users: SDK.user.id});

switch (data.type) {
  case 'list':
    if (!data.data) break;
    await Promise.all(data.data.map(async (d) => {
      d.resources = (await ProjectResources.findAll({project: d.id})).data();
    }));
    break;

  case 'view':
    data.data.resources = (await ProjectResources.findAll({project: data.data.id})).data();
    break;

  case 'create':
    if (data.prefix == 'pre') {
      data.data.account = account.id;
    }

    if (data.prefix == 'post') {
      let res = await Promise.all(data.data.resources.map(async (r) => {
        r.project = data.data.id
        return (await ProjectResources.create(r)).data();
      }));
    }
    break;

}
