let Accounts = await SDK.Table.get('accounts');

let token = await SDK.Auth.login(data, ['vk']);

let account = await Accounts.find({users: token.user.id}).catch(e => null);
if (!account) {
  account = await Accounts.create({
    name: '',
    email: '',
    users: [token.user.id]
  });
}

token.account = account;

return token;