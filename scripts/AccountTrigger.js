// { data, type, prefix, newData }
const UserResources = await SDK.Table.get('user_resources');
const Projects = await SDK.Table.get('projects');

switch (data.type) {
  case 'list':
    if (!data.data) break;
    await Promise.all(data.data.map(async (d) => {
      d.resources = (await UserResources.findAll({account: d.id})).data();
      d.projects = (await Projects.findAll({account: d.id})).data();
    }));
    break;

  case 'view':
    data.data.resources = (await UserResources.findAll({account: data.data.id})).data();
    data.data.projects = (await Projects.findAll({account: data.data.id})).data();
    break;

  case 'save':
    data.newData.resources = [];
    data.newData.projects = [];
    break;

  case 'delete':
    await UserResources.remove({account: data.data.id});
    await Projects.remove({account: data.data.id});
    break;

}
