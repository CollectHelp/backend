var config = require('./config');
config.application = require('./application');
config.entity = require('./entities');
module.exports = config;
