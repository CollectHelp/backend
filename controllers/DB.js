'use strict';

const SDK = require('sdk');
const Router = SDK.KoaRouter;
const router = new Router();
const tableRouter = new Router();
const Table = SDK.Table(new SDK());

const tag = 'Table';
const swagger = Table.swagger();
const auth = true;
const pagination = true;
const sort = true;
const required = true;

const access = (sdk) => {
  switch (sdk.request.method.toUpperCase()) {
    case 'GET': return sdk.request.params.data_id ? ['table', 'view'] : ['table', 'list'];
    case 'POST': return ['table', 'create'];
    case 'PUT': return ['table', 'update'];
    case 'DELETE': return ['table', 'delete'];
  }
};

router.get('/:data_id?', {
  tag, access, pagination, sort, swagger,
}, function *() {
  let options = {};
  let optionsList = [
    'sort', 'page', 'offset', 'limit',
    'populate', 'select',
    'crop', 'resize', 'scale', 'rotate'
  ];

  optionsList.map(p => {
    options[p] = this.request.query[p];
    delete this.request.query[p];
  });

  if (options.limit) {
    options.limit = parseInt(options.limit);
    options.page = options.page ? parseInt(options.page) : 0;
  }
  if (options.sort) Object.keys(options.sort).map(k => options.sort[k] = parseInt(options.sort[k]));

  let res, filter;
  if (this.params.data_id) {
    filter = this.getFilter({ id: this.params.data_id });
    res = yield this.table.find(filter, options, true);
  } else {
    filter = this.getFilter(this.request.query);
    res = yield this.table.findAll(filter, options, true);
  }

  this.body = res;
});

router.post('/', {
  tag, access, swagger
}, function *() {
  delete this.request.fields.id;

  Object.keys(this.request.fields).map(v => {
    let field = v;
    let value = this.request.fields[v];
    let match = field.match(/^([^\[]+)(\[(.*)\])?/);

    if (match.length > 2 && match[2]) {
      field = match[1];
      this.request.fields[field] = this.request.fields[field] || [];
      if (match[3] && (!this.request.fields[field][match[3]] || !Array.isArray(this.request.fields[field]))) {
        this.request.fields[field][match[3]] = value;
      } else {
        this.request.fields[field].push(value);
      }
    }
  });

  let res = yield this.table.create(this.request.fields);
  this.body = res;
});

router.put('/:data_id', {
  tag, access, swagger
}, function *() {
  this.request.query = this.request.query || {};
  Object.keys(this.request.fields).map(v => {
    let field = v;
    let value = this.request.fields[v];
    let match = field.match(/^([^\[]+)(\[(.*)\])?/);

    if (match.length > 2 && match[2]) {
      field = match[1];
      this.request.fields[field] = this.request.fields[field] || [];
      if (match[3] && (!this.request.fields[field][match[3]] || !Array.isArray(this.request.fields[field]))) {
        this.request.fields[field][match[3]] = value;
      } else {
        this.request.fields[field].push(value);
      }
    }
  });

  let filter = this.getFilter({ id: this.params.data_id });
  //let select = this.getFilterSelect(this.request.query.select);
  let res = yield this.table.find(filter, this.request.query);
  res = yield res.update(this.request.fields);
  this.body = res;
});

router.delete('/:data_id', {
  tag, access, swagger
}, function *() {
  this.request.query = this.request.query || {};
  let filter = this.getFilter({ id: this.params.data_id });
  let res = yield this.table.find(filter, this.request.query);
  res = yield res.remove();
  this.body = res;
});

tableRouter.use('/:table', function *(next) {
  if (!this.sdk) throw new this.sdk.Error(400, 'No application');
  this.table = yield this.sdk.Table.fetch(this.params.table);
  yield next;
}, router.routes());

module.exports = tableRouter;
