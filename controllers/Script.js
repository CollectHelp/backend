'use strict';

const Router = require('sdk').KoaRouter;
const router = new Router();
const scriptRouter = new Router();

const Script = require('sdk').Script;

const tag = 'Script';
const fieldScript = 'script';
const swaggerFunc = Script.swagger(fieldScript);
const [ auth ] = [true];

const access = ['script', 'run'];

router.all('/', {
  tag, auth, access, swaggerFunc,
}, function *() {
  if (!this.script || this.script.type != 'Script') throw new this.sdk.Error(400, 'Invalid entity.');

  let data = yield this.script.run();
  if (data instanceof Error) throw data;
  this.body = data;
});

scriptRouter.use('/:script', function *(next) {
  this.script = yield this.sdk.Script.fetch(this.params.script);
  yield next;
}, router.routes());

module.exports = scriptRouter;
