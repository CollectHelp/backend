'use strict';

const Router = require('sdk').KoaRouter;
const router = new Router();

const Role = require('sdk').Role;

const tag = 'Role';
const [ maybeApp, auth, pagination, sort, required ] = [true, true, true, true, true];

const access = (sdk) => {
  switch (sdk.request.method.toUpperCase()) {
    case 'GET': return sdk.request.params.role_id ? ['role', 'view'] : ['role', 'list'];
    case 'POST': return ['role', 'create'];
    case 'PUT': return ['role', 'update'];
    case 'DELETE': return ['role', 'delete'];
  }
};

// Get all or one role
router.get('/:role_id?', {
  tag, maybeApp, auth, access, pagination, sort,
  responses: {
    200: { description: 'OK', model: Role },
  }
}, function *() {
  if (this.params.role_id) {
    let role = yield this.sdk.Role.find(this.params.role_id);
    this.body = role;
    return;
  }

  let options = {};
  let optionsList = [
    'sort', 'page', 'offset', 'limit',
    'populate', 'select',
    'crop', 'resize', 'scale', 'rotate'
  ];

  optionsList.map(p => {
    options[p] = this.request.query[p];
    delete this.request.query[p];
  });

  if (options.limit) {
    options.limit = parseInt(options.limit);
    options.page = options.page ? parseInt(options.page) : 0;
  }
  if (options.sort) Object.keys(options.sort).map(k => options.sort[k] = parseInt(options.sort[k]));

  let { query = {} } = this.request;

  let roles = yield this.sdk.Role.findAll(query, options);
  this.body = roles;
});

// Create role
router.post('/', {
  tag, maybeApp, auth, access,
  body: Role.schema,
  responses: {
    200: { description: 'OK', model: Role }
  }
}, function *() {
  if (!this.request.fields || !this.request.fields.name) {
    this.status = 400;
    this.body = 'Name is required';
    return;
  }

  let role = new this.sdk.Role(this.request.fields);
  yield role.save();
  this.body = role;
});

// Update role
router.put('/:role_id', {
  tag, maybeApp, auth, access,
  body: Role.schema,
  responses: {
    200: { description: 'OK', model: Role },
  }
}, function *() {
  let { fields = {} } = this.request;

  if (!Object.keys(fields).length) {
    this.status = 400;
    this.body = 'Empty request';
    return;
  }

  let role = yield this.sdk.Role.find(this.params.role_id);
  Object.keys(fields).map(f => role[f] = fields[f]);
  role = yield role.save();
  this.body = role;
});

// Delete role
router.delete('/:role_id', {
  tag, maybeApp, auth, access,
  responses: {
    200: { description: 'OK' },
  }
}, function *() {
  let role = yield this.sdk.Role.find(this.params.role_id);
  role = yield role.remove();
  this.body = role;
});

module.exports = router;
