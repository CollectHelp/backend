'use strict';

const Router = require('sdk').KoaRouter;
const router = new Router();

const User = require('sdk').User;

const tag = 'User';
const [ maybeApp, auth, pagination, sort, required ] = [true, true, true, true, true];

const access = (sdk) => {
  switch (sdk.request.method.toUpperCase()) {
    case 'GET': return sdk.request.params.user_id ? ['user', 'view'] : ['user', 'list'];
    case 'POST': return ['user', 'create'];
    case 'PUT': return ['user', 'update'];
    case 'DELETE': return ['user', 'delete'];
  }
};

// Get all or one user
router.get('/:user_id?', {
  tag, access, maybeApp, pagination, sort,
  responses: {
    200: { description: 'OK', model: User },
  }
}, function *() {
  this.request.query = this.request.query || {};
  let filter, select;
  let options = {};
  let optionsList = [
    'sort', 'page', 'offset', 'limit',
    'populate', 'select',
    'crop', 'resize', 'scale', 'rotate'
  ];

  optionsList.map(p => {
    options[p] = this.request.query[p];
    delete this.request.query[p];
  });

  if (options.limit) {
    options.limit = parseInt(options.limit);
    options.page = options.page ? parseInt(options.page) : 0;
  }
  if (options.sort) Object.keys(options.sort).map(k => options.sort[k] = parseInt(options.sort[k]));
  select = this.getFilterSelect(options.select);
  options = Object.assign(options, { select });

  let Account = yield this.sdk.Table.get('accounts');

  if (this.params.user_id) {
    if (['me', 'my', 'self'].indexOf(this.params.user_id.toLowerCase()) >= 0) this.params.user_id = this.sdk.user.id;

    filter = this.getFilter({ id: this.params.user_id });

    let user = yield Account.find({ users: this.params.user_id }, options, true);
    if (!user) throw new this.sdk.Error(404, 'User not found');

    this.body = user;
    return;
  }

  let { query = {} } = this.request;

  filter = this.getFilter(query);

  let users = yield Account.findAll(filter, options, true);
  this.body = users;
});

// Create user
router.post('/', {
  tag, access, maybeApp,
  body: User.schema,
  responses: {
    200: { description: 'OK', model: User }
  }
}, function *() {
  if (!this.request.fields || !Object.keys(this.request.fields)) throw new this.sdk.Error(400, 'Empty request');
  delete this.request.fields.id;

  let user = yield this.sdk.User.create(this.request.fields);
  this.body = user;
});

// Update user
router.put('/:user_id', {
  tag, access, maybeApp,
  body: User.schema,
  responses: {
    200: { description: 'OK', model: User },
  }
}, function *() {
  let { fields = {} } = this.request;

  if (!Object.keys(fields).length) {
    this.status = 400;
    this.body = 'Empty request';
    return;
  }

  let filter = this.getFilter({ id: this.params.user_id });
  let user = yield this.sdk.User.find(filter);
  yield user.update(fields);
  this.body = user;
});

// Delete user
router.delete('/:user_id', {
  tag, access, maybeApp,
  responses: {
    200: { description: 'OK' },
  }
}, function *() {
  let filter = this.getFilter({ id: this.params.user_id });
  let user = yield this.sdk.User.find(filter);
  yield user.remove();
  this.body = {};
});

module.exports = router;
