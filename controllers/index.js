'use strict';

const router = require('koa-router')();

router.get('/ping', function *() {
  this.body = 'pong';
});

router.get('/time', function *() {
  let time = new Date();
  this.body = { ms: time.getTime(), date: time.toUTCString() };
});

router.use('/auth', require('./Auth').routes());
router.use('/user', require('./User').routes());
router.use('/role', require('./Role').routes());
router.use('/db', require('./DB').routes());
router.use('/service', require('./Service').routes());
router.use('/script', require('./Script').routes());

module.exports = router;
