'use strict';

const Router = require('sdk').KoaRouter;
const router = new Router();
const serviceRouter = new Router();

const Service = require('sdk').Service;

const tag = 'Service';
const fieldService = 'service';
const swaggerFunc = Service.swagger(fieldService);
const [ auth ] = [true];

const access = (sdk) => ['service', sdk.request.method.toUpperCase()];

router.all('/:path?', {
  tag, auth, access, swaggerFunc,
}, function *() {
  if (!this.service || this.service.type != 'Service') throw new this.sdk.Error(400, 'Invalid entity.');

  var route = this.service.findRoute(this);
  if (!route) throw new this.sdk.Error(404, 'Route not found');

  let data = yield this.service.request(route);

  this.status = data.statusCode;
  this.body = data.body;
});

serviceRouter.use('/:service', function *(next) {
  this.service = yield this.sdk.Service.fetch(this.params.service);
  yield next;
}, router.routes());

module.exports = serviceRouter;
