'use strict';

const Router = require('sdk').KoaRouter;
const router = new Router();

const tag = 'Auth';
const [ noApp, maybeApp, auth, pagination, sort, required ] = [true, true, true, true, true, true];

router.post('/register/:register_method?', {
  tag,
  responses: {
    200: { description: 'OK' },
  }
}, function *() {
  let data = yield this.sdk.Auth.register(this.request.fields, this.params.register_method);
  this.body = data && data.toObject ? data.toObject() : data;
});

router.post('/login/:login_method?', {
  tag, maybeApp
}, function *() {
  let data = yield this.sdk.Auth.login(this.request.fields, this.params.login_method);
  this.body = Object.assign({}, data, { user: data.user.id });
});

router.post('/logout/:logout_method?', {
  tag, auth
}, function *() {
  let data = yield this.sdk.Auth.logout(this.request.fields, this.params.logout_method);
  this.body = data;
});

router.post('/refresh/:refresh_method?', {
  tag,
}, function *() {
  let data = yield this.sdk.Auth.refresh(this.request.fields, this.params.refresh_method);
  this.body = Object.assign({}, data, { user: data.user.id });
});

router.get('/check', {
  tag,
}, function *() {
  this.body = Object.assign({}, this.sdk.accessToken, { user: this.sdk.user.toObject() });
});

module.exports = router;
