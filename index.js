const koa = require('koa');
const logger = require('koa-morgan');
const router = require('koa-router')();
const cors = require('koa-cors');
const body = require('koa-better-body');
const SDK = require('sdk');

const config = require('./config');
const app = koa();

app.name = config.name;
SDK.configure(Object.assign(config, { rootDir: __dirname }));

app.use(function *(next) {
  this.path = this.path.replace(/\/\//g, '/');
  this.path = this.path.replace(/^\/api\//i, '/');
  if (!this.headers.admin) {
    this.headers['x-collect-help-id'] = '58de60c743aa6226468f5e0b';
  }
  this.sdk = SDK;

  yield next;
});

require('koa-qs')(app, 'extended');
app.use(logger.middleware(config.logFormat));
app.use(cors());
app.use(body({ querystring: require('qs') }));
app.use(SDK.middlewareKoa('x-collect-help-id'));

app.use(require('./controllers').routes());

app.on('error', (err, ctx) => {
  console.error('server error', err, ctx);
});

app.server = app.listen(config.port, () => {
  console.log('Server started on 127.0.0.1:' + config.port);
});

process.on('unhandledRejection', (err) => {
  console.error('UNHANDLED REJECTION', err);
});

module.exports = app;
